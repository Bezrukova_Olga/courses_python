from tkinter import *
import psycopg2
from tkinter import ttk

def add_new_course(pk, name, time_duration, price, teacher, program_of_course):
    sql = """INSERT INTO course(id, name, time_duration, price, teacher, program_of_course)
                 VALUES(%s,%s,%s,%s,%s,%s);"""
    record_to_insert = (pk, name, time_duration, price, teacher, program_of_course)
    try:
        connection = psycopg2.connect(user="postgres",
                                      password="admin",
                                      host="localhost",
                                      port="5433",
                                      database="Education")

        cursor = connection.cursor()
        cursor.execute(sql, record_to_insert)
        connection.commit()

    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL: ", error)
    finally:
        # closing database connection.
        if (connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")


# def clicked():
#     res = "Привет {}".format(txt.get())
#     lbl.configure(text=res)

def sign_up_for_a_course():
    lbl = Label(tab2, text="Fill out the form to register for the course", font=("Arial Bold", 10))
    lbl.configure(anchor="center")
    lbl.grid(column=0, row=0)
    return lbl

window = Tk()
window.title("Дополнительное образование")
window.geometry('400x250')
tab_control = ttk.Notebook(window)
tab1 = ttk.Frame(tab_control)
tab2 = ttk.Frame(tab_control)
tab_control.add(tab1, text='Первая')
tab_control.add(tab2, text='Вторая')
lbl1 = Label(tab1, text='Вкладка 1')
lbl1.grid(column=0, row=0)
lbl2 = sign_up_for_a_course()
lbl2.grid(column=0, row=0)
tab_control.pack(expand=1, fill='both')
# lbl = Label(window, text="Привет", font=("Arial Bold", 10))
# lbl.grid(column=0, row=0)
# txt = Entry(window, width=10)
# txt.focus()
# txt.grid(column=1, row=0)
# btn = Button(window, text="Не нажимать!", command=clicked)
# btn.grid(column=2, row=0)
window.mainloop()
# add_new_course(2, 'Computer graphics', 90.0, 12000.0, 'Medvedev', '1) Introductory lesson\n2) Basic concepts\n3) First practical lesson\n4) Shaders\n5) Final lesson')


